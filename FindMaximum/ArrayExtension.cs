﻿using System;

namespace FindMaximumTask
{
    public static class ArrayExtension
    {
        public static int FindMaximum(int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (array.Length == 0)
            {
                throw new ArgumentException("Array is empty.");
            }

            return Maxsimum(array, 0, array.Length);
        }

        public static int Maxsimum(int[] source, int startRange, int endRange)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (endRange - startRange == 1)
            {
                return source[startRange];
            }

            int middle = (startRange + endRange) / 2;
            int leftPart = Maxsimum(source, startRange, middle);
            int rightPart = Maxsimum(source, middle, endRange);
            return (leftPart > rightPart) ? leftPart : rightPart;
        }
    }
}
